﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Passez en revue les valeurs des attributs de l'assembly

<Assembly: AssemblyTitle("Lanceur Minecraft")> 
<Assembly: AssemblyDescription("Lanceur Minecraft opensource en VisualBasic .NET. Ce produit est sous licence Créative Common : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 3.0 France. Les textures du lanceur (arrière plan) et le logo minecraft sont la propriété de Mojang.")> 
<Assembly: AssemblyCompany("Intrepide-Prod")> 
<Assembly: AssemblyProduct("Lanceur Minecraft")> 
<Assembly: AssemblyCopyright("Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("cb03d046-2a85-4d70-b841-88f6e797309f")> 

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.3.0")> 
<Assembly: AssemblyFileVersion("1.0.3.0")> 
