﻿Imports System.IO

'Ce logiciel est sous licence Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)
'Pour plus d'information sur cette licence naviguez sur cette pas : http://creativecommons.org/licenses/by-nc-sa/3.0/fr/
'
'Le logiciel Lanceur Minecraft est développé par hmsintrepide

Module xml

    'Zone des variables : modifiable à souhait
#Region "variable modifiable"

    'Emplacement du dossier minecraft
    'Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) = %appdata%
    Public mcroot As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\.minecraft\"
    'Lien des fichiers minecraft (sur le serveur de mise à jour)
    Public urlroot As String = "http://s3.amazonaws.com/MinecraftDownload/"
    'Lien de la page web de démarrage (mcupdate.tumblr.com)
    Public urlweb As String = "http://mcupdate.tumblr.com"
    'Memoire maximum de la machine virtuel java
    Public xmx As String = "1024M"
    'Memoire minimum de la machine virtuel java
    Public xms As String = "512M"
    'Affiche ou non la console de debug
    Public show_debug As String = "0"
    'Active ou désactive la case "enregistrer mot de passe" par defaut
    Public savewd As String = "0"

#End Region

    'Zone de variable fixe, à ne pas modifier
#Region "variable system"

    Public login As String = mainform.TextBox1.Text
    Public passwd As String = mainform.TextBox2.Text
    Public update_version As String = My.Application.Info.Version.ToString
    Public pseudo As String = login
    Public alternate As String = "0"
    Public table_update()
    Public sessionid As String

#End Region

    'Lit le fichier option du launcher
    Sub read_option()

        Dim sr As New StreamReader(mcroot & "launcher\option")
        urlroot = sr.ReadLine
        urlweb = sr.ReadLine
        show_debug = sr.ReadLine
        xmx = sr.ReadLine
        xms = sr.ReadLine
        login = sr.ReadLine
        passwd = sr.ReadLine
        savewd = sr.ReadLine
        alternate = sr.ReadLine
        sr.Close()

    End Sub

    'Ecrit le fichier option du launcher
    Sub write_option()

        Dim sw As New StreamWriter(mcroot & "launcher\option")
        sw.WriteLine(urlroot)
        sw.WriteLine(urlweb)
        sw.WriteLine(show_debug)
        sw.WriteLine(xmx)
        sw.WriteLine(xms)
        sw.WriteLine(login)
        sw.WriteLine(passwd)
        sw.WriteLine(savewd)
        sw.WriteLine(alternate)
        sw.Close()

    End Sub

End Module
