﻿Imports System.IO
Imports System.Net

'Ce logiciel est sous licence Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)
'Pour plus d'information sur cette licence naviguez sur cette pas : http://creativecommons.org/licenses/by-nc-sa/3.0/fr/
'
'Le logiciel Lanceur Minecraft est développé par hmsintrepide

Module dlminecraft

    'Demande si la mise à jour doit être effectué
    Public Sub check_dlminecraft()

        check_web("Voulez vous mettre à jour minecraft ? Attention les mods installé seront perdu ! </br><a href=""#allow_update"">Oui</a> <a href=""#deny_update"">Non</a>")
        pause(100)

    End Sub

    'Ce produit quand "oui" est répondu à la demande de mise à jour
    'Cette fonction est exécuté par WebBrowser1.Navigated sur mainform
    Function download_minecraft()

        'Affiche et télécharge les fichiers minecraft et lwjgl
        check_web("Téléchargement de : minecraft.jar")
        pause(100)
        DownloadFile(urlroot & "minecraft.jar", mcroot & "bin\minecraft.jar")

        check_web("Téléchargement de : jinput.jar")
        pause(100)
        DownloadFile(urlroot & "jinput.jar", mcroot & "bin\jinput.jar")

        check_web("Téléchargement de : lwjgl.jar")
        pause(100)
        DownloadFile(urlroot & "lwjgl.jar", mcroot & "bin\lwjgl.jar")

        check_web("Téléchargement de : lwjgl_util.jar")
        pause(100)
        DownloadFile(urlroot & "lwjgl_util.jar", mcroot & "bin\lwjgl_util.jar")

        check_web("Téléchargement de : windows_natives.jar")
        pause(100)
        DownloadFile(urlroot & "windows_natives.jar", mcroot & "bin\natives\windows_natives.jar")

        'Extrait 7zip
        check_web("Extraction de : 7zip")
        pause(100)
        If Not File.Exists(mcroot & "bin\natives\sevenzip.exe") Then
            My.Computer.FileSystem.WriteAllBytes(mcroot & "bin\natives\sevenzip.exe", My.Resources.sevenzip, False)
        End If

        'Extrait les librairies lwjgl avec zip
        check_web("Extraction de : windows_natives.jar")
        pause(100)
        Shell(mcroot & "bin\natives\sevenzip.exe e -y -o" & mcroot & "bin\natives " & mcroot & "bin\natives\windows_natives.jar")

        pause(2000)

        'Supprime les fichiers inutiles
        check_web("Suppression du cache ...")
        pause(100)
        Try
            My.Computer.FileSystem.DeleteDirectory(mcroot & "bin\natives\META-INF\", FileIO.DeleteDirectoryOption.DeleteAllContents)
            File.Delete(mcroot & "bin\natives\MANIFEST.MF")
            File.Delete(mcroot & "bin\natives\MOJANG_C.DSA")
            File.Delete(mcroot & "bin\natives\MOJANG_C.SF")
            File.Delete(mcroot & "bin\natives\sevenzip.exe")
            File.Delete(mcroot & "bin\natives\windows_natives.jar")
        Catch
        End Try
        pause(100)

        'Télécharge et enregistre la longeur du fichier minecraft.jar ce qui permet la vérification de mise à jour
        Dim req As System.Net.HttpWebRequest = DirectCast(HttpWebRequest.Create(New Uri(urlroot & "minecraft.jar")), System.Net.HttpWebRequest)
        Dim sw As New StreamWriter(mcroot & "launcher\version")
        sw.WriteLine(aversion)
        sw.Close()

        check_web("Mise à jour terminé !")
        pause(1000)

        'module mcinstall
        check_mcinstall()
        'module minecraft
        start_minecraft()

        Return Nothing
    End Function

End Module
