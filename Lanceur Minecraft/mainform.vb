﻿'Ce logiciel est sous licence Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)
'Pour plus d'information sur cette licence naviguez sur cette pas : http://creativecommons.org/licenses/by-nc-sa/3.0/fr/
'
'Le logiciel Lanceur Minecraft est développé par hmsintrepide

Imports System.Text

Public Class mainform

    Private Sub main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'module fonction
        verify()

        'Chargement du des images et de l'icone
        Me.Icon = My.Resources.favicon
        Me.BackgroundImage = My.Resources.form_background
        PictureBox1.BackgroundImage = My.Resources.bottom_bar
        PictureBox2.Image = My.Resources.logo

        'Chargement du navigateur
        WebBrowser1.Url = New Uri(urlweb)
        pause(100)

        'module java
        check_java()

        'Chargement des données de connexion
        TextBox1.Text = login
        'Décode le mot de passe
        Dim decodedBytes As Byte()
        decodedBytes = Convert.FromBase64String(passwd)
        TextBox2.Text = Encoding.UTF8.GetString(decodedBytes)
        If savewd = "1" Then
            CheckBox1.Checked = True
        End If

        'module update
        check_update()

    End Sub

    'Ce produit au clic sur le bouton connexion
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        'Enregistrement des données de connexion
        check_web("Chargement ...")
        login = TextBox1.Text
        'Vérifie si l'option "enregistrer le mot de passe" est activé
        If CheckBox1.Checked = True Then
            'Code le mot de passe
            Dim bytesToEncode = Encoding.UTF8.GetBytes(TextBox2.Text)
            passwd = Convert.ToBase64String(bytesToEncode)
            savewd = "1"
        Else
            passwd = ""
        End If

        'Module option
        write_option()

        'Vérification du compte minecraft
        'module account
        If check_account() = True Then
            'Module mcupdate
            check_mcupdate()
        Else
            'Affiche une popup dans le navigateur
            check_web("Vous n'êtes pas premium !")
        End If

    End Sub

    'Ce produit au clic sur le lien compte minecraft
    Private Sub LinkLabel1_LinkClicked(ByVal sender As Object, ByVal e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked

        'Lien vers minecraft.net
        WebBrowser1.Url = New Uri("https://minecraft.net/")

    End Sub

    'Ce produit au clic sur le bouton options
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click

        'Lance la fenêtre options
        parametre.ShowDialog()

    End Sub

    'Ce produit au chargement d'un page par le navigateur
    Private Sub WebBrowser1_DocumentCompleted(ByVal sender As Object, ByVal e As WebBrowserNavigatedEventArgs) Handles WebBrowser1.Navigated

        'Vérifie la réponse du message de mise à jour
        If WebBrowser1.Url.ToString = "about:blank#allow_update" Then
            'module dlminecraft
            download_minecraft()
        ElseIf WebBrowser1.Url.ToString = "about:blank#deny_update" Then
            'module mcinstall
            check_mcinstall()
            'module minecraft
            start_minecraft()
        End If

    End Sub

End Class