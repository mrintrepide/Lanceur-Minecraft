﻿Imports System.IO

'Ce logiciel est sous licence Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)
'Pour plus d'information sur cette licence naviguez sur cette pas : http://creativecommons.org/licenses/by-nc-sa/3.0/fr/
'
'Le logiciel Lanceur Minecraft est développé par hmsintrepide

Module fonction

    'Fonction permetant de créer des pauses
    'L'éxécution de fonction, freeze le navigateur et empêche l'affichage des pop-up
    Public Function pause(ByVal sec As Integer)

        Dim endwait As Double
        endwait = Environment.TickCount + sec
        While Environment.TickCount < endwait
            System.Threading.Thread.Sleep(1)
            Application.DoEvents()
        End While

        Return Nothing
    End Function

    'Fonction qui affiche un message dans le navigateur (demande de mise à jour, chargement, ...)
    Public Function check_web(ByVal content As String)

        mainform.WebBrowser1.DocumentText = "<html><head><title>mcupdate</title>" & _
        "<style>body {font-family:sans-serif;background-color:#222222;" & _
        "background-image:url('http://static.tumblr.com/qfn3sex/ezom7y7iq/bg_main.png');color:#e0d0d0;text-align:center;}" & _
        "a{color:#aaaaff;}hr{border:0;color:#111111;background-color:#111111;height:2px;}h3{color:#ffffff;font-size:16px;}" & _
        "img{border:0;margin:0;}</style>" _
        & "</head><body><div><p><a href=""" & urlweb & """ style=""float:left;"">Retour</a><h3>Lanceur Minecraft</h3></p></div><hr><div><p>" _
        & content & "</p></div></body></html>"

        Return Nothing
    End Function

    'Verifie la présence de dossier et de fichier
    Public Function verify()

        'existance du dossier .minecraft
        If Not Directory.Exists(mcroot) Then
            Directory.CreateDirectory(mcroot)
        End If

        'existance du dossier launcher
        If Not Directory.Exists(mcroot & "launcher") Then
            Directory.CreateDirectory(mcroot & "launcher")
        End If

        'existance du fichier option
        If Not File.Exists(mcroot & "launcher\option") Then
            'module option
            write_option()
        Else
            'module option
            read_option()
        End If

        Return Nothing
    End Function

    'Télécharge les fichiers et les enregistre à la place demander, écrit par dessus les fichiers existant, timeout à 1s
    Function DownloadFile(ByVal sin As String, ByVal sout As String)

        My.Computer.Network.DownloadFile(sin, sout, "", "", False, 1000, True)

        Return Nothing
    End Function

End Module
