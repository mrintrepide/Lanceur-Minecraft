﻿Imports System.IO

'Ce logiciel est sous licence Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)
'Pour plus d'information sur cette licence naviguez sur cette pas : http://creativecommons.org/licenses/by-nc-sa/3.0/fr/
'
'Le logiciel Lanceur Minecraft est développé par hmsintrepide

Module update

    'Vérifie les mises à jour du lanceur
    'à ne modifier que si vous utiliser un version custom du lanceur
    Public Sub check_update()

        Try
            DownloadFile("http://hms.craft-web.com/mcupdate/files/update", mcroot & "launcher\update")

            Dim sr As New StreamReader(mcroot & "launcher\update")
            Dim ver = sr.ReadLine()
            sr.Close()

            If Not ver = update_version Then
                check_web("Une nouvelle version du lanceur est disponible. Vous pouvez allez la télécharger ici : <a href=""http://hms.craft-web.com/mcupdate/index.html"">http://hms.craft-web.com/mcupdate/index.html</a>")
                pause(100)
            End If

        Catch ex As Exception
            check_web("Impossible vérifier si une mise à jour du lanceur est diponible")
            pause(100)
        End Try

    End Sub

End Module
