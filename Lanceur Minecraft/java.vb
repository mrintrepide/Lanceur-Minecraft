﻿Imports System.IO

'Ce logiciel est sous licence Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)
'Pour plus d'information sur cette licence naviguez sur cette pas : http://creativecommons.org/licenses/by-nc-sa/3.0/fr/
'
'Le logiciel Lanceur Minecraft est développé par hmsintrepide

Module java

    'Verifie l'existance de java 6 ou 7 sur les machine 32 ou 64 bit
    Public Sub check_java()

        If Not File.Exists("C:\Program Files\Java\jre7\bin\java.exe") And _
            Not File.Exists("C:\Program Files\Java\jre6\bin\java.exe") And _
            Not File.Exists("C:\Program Files (x86)\Java\jre7\bin\java.exe") And _
            Not File.Exists("C:\Program Files (x86)\Java\jre6\bin\java.exe") Then
            'Affiche un message d'erreur
            check_web("Minecraft requière Java, veullez le télécharger à l'adresse www.java.fr")
            mainform.Close()
        End If

    End Sub

End Module
