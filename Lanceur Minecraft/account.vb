﻿'Ce logiciel est sous licence Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)
'Pour plus d'information sur cette licence naviguez sur cette pas : http://creativecommons.org/licenses/by-nc-sa/3.0/fr/
'
'Le logiciel Lanceur Minecraft est développé par hmsintrepide

Module account

    'Créer une instance navigateur
    Dim WebInstance As New WebBrowser
    'Variable de la fonction
    Dim result As Boolean = False

    'Cette fonction vérifie si le compte est prémium et retourne vrais ou faux
    Function check_account()

        WebInstance.Url = New Uri("https://login.minecraft.net/?user=" & mainform.TextBox1.Text & "&password=" & mainform.TextBox2.Text & "&version=13")

        'On attend le chargement
        While WebInstance.IsBusy
            pause(100)
        End While

        'petit pause au cas ou, bug string vide
        pause(2000)

        'Vérification des donné
        Try
            'Analise les données a la recherche du nom du joueur
            Dim table() As String = Split(WebInstance.DocumentText.ToString, ":")
            table_update = Split(table(0), "<PRE>")
            'Enregistre le pseudo != mojang login, il est placé en 3ème position
            pseudo = table(2)
            sessionid = table(3)
            result = True
            'Cherche l'existance du mot "deprecated", il est placé en 2ème position
            If table(1) = "deprecated" Then
                result = True
            Else
                result = False
            End If
        Catch ex As Exception
            result = False
        End Try

        Return result
    End Function

End Module
