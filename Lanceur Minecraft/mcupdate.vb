﻿Imports System.IO

'Ce logiciel est sous licence Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)
'Pour plus d'information sur cette licence naviguez sur cette pas : http://creativecommons.org/licenses/by-nc-sa/3.0/fr/
'
'Le logiciel Lanceur Minecraft est développé par hmsintrepide

Module mcupdate

    Public aversion As String = table_update(1)

    'Cherche les mises à jour diponible
    Public Sub check_mcupdate()

        'Verifie si minecraft est installé
        If File.Exists(mcroot & "bin\minecraft.jar") And File.Exists(mcroot & "launcher\version") Then

            'Lit la longeur du fichier minecraft.jar de la dernière mise à jour
            Dim sr As New StreamReader(mcroot & "launcher\version")
            Dim nversion As String = sr.ReadLine
            sr.Close()

            'Compare les deux valeurs
            If Not aversion = nversion Then
                'module dlminecraft
                check_dlminecraft()
            Else
                'module mcinstall
                check_mcinstall()
                'module minecraft
                start_minecraft()
            End If

        Else
            'module mcinstall
            check_mcinstall()
            'module dlminecraft
            check_dlminecraft()
        End If

    End Sub

End Module