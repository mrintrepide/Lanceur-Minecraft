﻿Imports System.Windows.Forms

'Ce logiciel est sous licence Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)
'Pour plus d'information sur cette licence naviguez sur cette pas : http://creativecommons.org/licenses/by-nc-sa/3.0/fr/
'
'Le logiciel Lanceur Minecraft est développé par hmsintrepide

Public Class parametre

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        If CheckBox1.Checked = True Then
            check_dlminecraft()
        End If
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As Object, ByVal e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Process.Start(mcroot)
    End Sub

    Private Sub parametre_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LinkLabel1.Text = mcroot
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As Object, ByVal e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        AboutBox1.ShowDialog()
    End Sub

End Class
