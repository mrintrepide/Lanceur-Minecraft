﻿Imports System.IO

'Ce logiciel est sous licence Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)
'Pour plus d'information sur cette licence naviguez sur cette pas : http://creativecommons.org/licenses/by-nc-sa/3.0/fr/
'
'Le logiciel Lanceur Minecraft est développé par hmsintrepide

Module minecraft

    'Réatribut les noms des objets de mainform
    Dim TextBox1 As TextBox = mainform.TextBox1
    Dim textBox2 As TextBox = mainform.TextBox2

    'Lance minecraft, en testant chaque version de java et défini si il doit afficher la console de debug ou non
    Public Sub start_minecraft()

        'Vérifie si java existe
        If File.Exists("C:\Program Files\Java\jre7\bin\java.exe") Then

            'Créer le processus
            Dim p = New ProcessStartInfo("C:\Program Files\Java\jre7\bin\java.exe", "-cp " & mcroot & "bin/minecraft.jar;" & mcroot & "bin/lwjgl.jar;" & mcroot & "bin/lwjgl_util.jar;" & mcroot & "bin/jinput.jar; -Djava.library.path=" & mcroot & "bin\natives -Xmx" & xmx & " -Xms" & xms & " net.minecraft.client.Minecraft " & pseudo & " " & sessionid)
            'Vérifie si on doit afficher la console
            If show_debug = "0" Then
                p.WindowStyle = ProcessWindowStyle.Hidden
            Else
                p.WindowStyle = ProcessWindowStyle.Normal
            End If
            'Démarre le processus
            Process.Start(p)

        ElseIf File.Exists("C:\Program Files\Java\jre6\bin\java.exe") Then

            Dim p = New ProcessStartInfo("C:\Program Files\Java\jre6\bin\java.exe", "-cp " & mcroot & "bin/minecraft.jar;" & mcroot & "bin/lwjgl.jar;" & mcroot & "bin/lwjgl_util.jar;" & mcroot & "bin/jinput.jar; -Djava.library.path=" & mcroot & "bin\natives -Xmx" & xmx & " -Xms" & xms & " net.minecraft.client.Minecraft " & pseudo & " " & sessionid)
            If show_debug = "0" Then
                p.WindowStyle = ProcessWindowStyle.Hidden
            Else
                p.WindowStyle = ProcessWindowStyle.Normal
            End If
            Process.Start(p)

        ElseIf File.Exists("C:\Program Files (x86)\Java\jre7\bin\java.exe") Then

            Dim p = New ProcessStartInfo("C:\Program Files (x86)\Java\jre7\bin\java.exe", "-cp " & mcroot & "bin/minecraft.jar;" & mcroot & "bin/lwjgl.jar;" & mcroot & "bin/lwjgl_util.jar;" & mcroot & "bin/jinput.jar; -Djava.library.path=" & mcroot & "bin\natives -Xmx" & xmx & " -Xms" & xms & " net.minecraft.client.Minecraft " & pseudo & " " & sessionid)
            If show_debug = "0" Then
                p.WindowStyle = ProcessWindowStyle.Hidden
            Else
                p.WindowStyle = ProcessWindowStyle.Normal
            End If
            Process.Start(p)

        ElseIf File.Exists("C:\Program Files (x86)\Java\jre6\bin\java.exe") Then

            Dim p = New ProcessStartInfo("C:\Program Files (x86)\Java\jre6\bin\java.exe", "-cp " & mcroot & "bin/minecraft.jar;" & mcroot & "bin/lwjgl.jar;" & mcroot & "bin/lwjgl_util.jar;" & mcroot & "bin/jinput.jar; -Djava.library.path=" & mcroot & "bin\natives -Xmx" & xmx & " -Xms" & xms & " net.minecraft.client.Minecraft " & pseudo & " " & sessionid)
            If show_debug = "0" Then
                p.WindowStyle = ProcessWindowStyle.Hidden
            Else
                p.WindowStyle = ProcessWindowStyle.Normal
            End If
            Process.Start(p)

        End If

        'Ferme le lanceur, fin du programme
        mainform.Close()

    End Sub

End Module
