﻿Imports System.IO

'Ce logiciel est sous licence Creative Common by-nc-sa (CC BY-NC-SA 3.0 FR)
'Pour plus d'information sur cette licence naviguez sur cette pas : http://creativecommons.org/licenses/by-nc-sa/3.0/fr/
'
'Le logiciel Lanceur Minecraft est développé par hmsintrepide

Module mcinstall

    'Vérifie les fichiers minecraft et lwjgl
    Public Sub check_mcinstall()

        If Not File.Exists(mcroot & "bin\jinput.jar") And Not File.Exists(mcroot & "bin\lwjgl.jar") _
        And Not File.Exists(mcroot & "bin\lwjgl_util.jar") And Not File.Exists(mcroot & "bin\minecraft.jar") _
        And Not File.Exists(mcroot & "bin\natives\jinput-dx8.dll") And Not File.Exists(mcroot & "bin\natives\jinput-dx8_64.dll") _
        And Not File.Exists(mcroot & "bin\natives\jinput-raw.dll") And Not File.Exists(mcroot & "bin\natives\jinput-raw_64.dll") _
        And Not File.Exists(mcroot & "bin\natives\lwjgl.dll") And Not File.Exists(mcroot & "bin\natives\lwjgl64.dll") _
        And Not File.Exists(mcroot & "bin\natives\OpenAL32.dll") And Not File.Exists(mcroot & "bin\natives\OpenAL64.dll") Then
            'module dlminecraft
            check_dlminecraft()
        End If

    End Sub

End Module
