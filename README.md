Lanceur-Minecraft 1.0.3 OUTATED
=================

Le nouveau launcher minecraft rend les anciens launcher HS.

Lanceur Minecraft opensource en VisualBasic .NET.

Ce produit est sous licence Créative Common 3.0 France :
- Attribution
- Pas d’Utilisation Commerciale
- Partage dans les Mêmes Conditions.

Les textures du lanceur (arrière plan) et le logo minecraft sont la propriété de Mojang.

1.0.3 : Fix 'bad login' message with online server

1.0.2 : Better update verification

1.0.1 : Premium verification bug

1.0.0 : Initial Version
